package com.mindtree.guest.book.api.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.guest.book.api.model.GuestBook;
import com.mindtree.guest.book.api.repo.GuestBookRepository;

@RestController
@RequestMapping("guestbook")
public class GuestBookController {

	@Autowired
	private GuestBookRepository repo;

	@GetMapping
	public List<GuestBook> getAllGuestBook() {
		return repo.findAll();
	}

	@PostMapping
	public GuestBook save(@RequestBody GuestBook guestBook) {
		guestBook.setCreatedDate(new Date());
		return repo.save(guestBook);
	}

}
