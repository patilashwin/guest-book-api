package com.mindtree.guest.book.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.guest.book.api.model.GuestBook;

@Repository
public interface GuestBookRepository extends JpaRepository<GuestBook, Integer> {

}
