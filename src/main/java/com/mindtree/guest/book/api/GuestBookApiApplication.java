package com.mindtree.guest.book.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuestBookApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GuestBookApiApplication.class, args);
	}

}
