FROM openjdk:8-jre
VOLUME /tmp
WORKDIR /data
COPY target/guest-book-api.jar  /data
EXPOSE 4000
ENTRYPOINT ["java" ,"-jar", "guest-book-api.jar"]